# MetaMobilePicker

Version 0.2.0
Pipeline identifying MGEs in metagenomic data

MetaMobilePicker identifies Mobile Genetic Elements (MGEs) in metagenomics reads.

## Installation
The best way to install MetaMobilePicker is using conda. Create a new conda environment using the command:
```
conda create --name metamobilepicker
conda activate metamobilepicker
```
Next, install the following dependencies:
```
conda install mamba
mamba install snakemake
conda install click
conda install singularity
```


## Testing the Pipeline
### Techinical test
After installation, the fastest way to test the installation is to use the included test data. This dataset consists of 10.000 reads and should run relatively fast.
To test the pipeline, run the following commands:
```
python metamobilepicker.py run --dryrun --config config/CI_config.yaml
```
If this doesn't give any errors, run the pipeline with the following command
```
python metamobilepicker.py run --config config/CI_config.yaml
```

### Actual dataset
To test the pipeline on an actual dataset, download [this](https://www.ebi.ac.uk/ena/browser/view/ERR2984773) mock community dataset and place the data in the data/raw/ directory. Next open the file src/config/config.yml file in a text editor and add a sample like in the example, or remove the '#' in front of the test sample.
Downloading the test data can also be done from within the src/ directory, using:
```
python metamobilepicker.py download
```
Next, to test the installation of the pipeline, run the following command from within the src directory:
```
python metamobilepicker.py run --dryrun --test
```
If this doesn't give errors, go ahead and run 
```
python metamobilepicker.py run --test
```

### Snakemake profiles
If you have a HPC with a SLURM scheduler available, run the following command to have snakemake submit the jobs to the HPC:
```
python metamobilepicker.py run -s
``` 
This will still run the skeleton of the pipeline in your current session. If this is an issue with expiring sessions or closing terminal windows, use the following command to also submit the skeleton to the HPC:
```
python metamobilepicker.py run -s -S
```
### Other options
In the events of a failing pipeline, the directory will get locked. Use the following command before retrying:
```
python metamobilepicker.py run --unlock
```
If you wish to run the pipeline with more cores than the default of 1 without running on an HPC, you can add them using the '--cores [cores]' option.



