#!/usr/bin/python
import click
import subprocess
import logging
import os
import urllib.request
from datetime import date

"""
Start script for the MetaMobilePicker Pipeline. 
This script uses the user supplied parameters to start the pipeline.
"""

def getRunName(outdir, callname="demessifier"):
    """
    Function returns a runName based on today's date and generates a unique name if today's date is already taken.
    """
    if not os.path.isdir("{}/{}".format(outdir,callname)):
        return(callname)
    today=date.today()
    default_name="{}_{}_{}_{}".format(callname, today.year,today.month,today.day)
    count = 1
    outname = default_name

    while True:
        if os.path.isdir("{}/{}".format(outdir,outname)):
            outname = "{}_{}".format(default_name,count)
            count += 1
        else:
            break
    return(outname)

def goodbye():
    # Returns exit message
    print("Thanks for using MetaMobilePicker!")

# Add date and time to log messages
logging.basicConfig(
    level=logging.INFO,
    datefmt="%Y-%m-%d %H:%M",
    format="[%(asctime)s %(levelname)s] %(message)s",
)

# Add the 'run' parameter group
@click.group()
def cligroup():
    """
    #### MetaMobilePicker ####

    Identification of Mobile Genetic Elements in metagenomics samples.
    """
@cligroup.command(
    "download",
    short_help="Download test data and Eggnog database"
)

@click.option(
    "-d","--database",
    is_flag=True,
    help="Download the necessary database"
)

@click.option(
    "-t","--testdata",
    is_flag=True,
    help="Download test dataset"
)

def download_data(database, testdata):
    if not database and not testdata:
        testdata = True
        database = True
    print("Downloading files, this might take a while")
    if testdata:
        file_1 = "../data/raw/ERR2241639_1_test.fastq.gz"
        file_2 = "../data/raw/ERR2241639_2_test.fastq.gz"
        # download test data
        if not os.path.exists(file_1):
            print("Downloading testfile 1")
            url_1 = "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR224/009/ERR2241639/ERR2241639_1.fastq.gz"
            urllib.request.urlretrieve(url_1, file_1)
        if not os.path.exists(file_2):
            url_2 = "ftp://ftp.sra.ebi.ac.uk/vol1/fastq/ERR224/009/ERR2241639/ERR2241639_2.fastq.gz"
            print("Downloading testfile 2")
            urllib.request.urlretrieve(url_2, "../data/raw/ERR2241639_2_test.fastq.gz")
        else:
            print("Test data is already downloaded")
    """
    # Downloading the eggnog database doesn't really work. Eggnog has it's own script for it, and 
    # it requires the full package to be installed as dep. I don't think I want that. Ideas welcome ;)
    # Also, the db is already downloaded in the main run, it's just nice to be able to do it 'manually'
    # too, to avoid doing it in runtime.
    if database:
        if not os.path.exists("../data/raw/eggnog2/eggnog.db"):
            print("Downloading database")
            subprocess.call(["python","./scripts/download_eggnog_data.py","--data-dir","../data/raw/eggnog2/","-y"])
        else:
            print("Database already exists")         
    """
@cligroup.command(
    "run",
    short_help="Run MetaMobilePicker"
)
# Dryrun option
@click.option(
    "-n","--dryrun",
    is_flag=True,
    default=False,
    show_default=True,
    help="Test the script without running the pipeline"
)
# Specify Snakefile
@click.option(
    "-s","--snakefile",
    default="Snakefile",
    show_default=True,
    help="Change the name of the Snakefile to be used"
)

# Use Snakemake profiles (TODO)
@click.option(
    "-p","--profile",
    default=None
)

# Specify config file
@click.option(
    "-c","--config",
    default="config/config.yaml",
    show_default=True
)
@click.option(
    "-s","--sbatch",
    is_flag=True,
    default=False,
    show_default=True,
    help="Run MetaMobilePicker on a SLURM cluser (BETA)"
)
@click.option(
    "-u","--unlock",
    is_flag=True,
    default=False,
    show_default=False,
    help="Unlock directory after failed Snakemake attempt"
)

#@click.option(
#    "-u","--unlock",
#    is_flag=True
#    default=False,
#    help="Unlocks the output directory after an error only use when Snakemake says so."
#)
# Run on predetermined test data (overrides config option)
@click.option(
    "-t","--test",
    is_flag=True,
    default=False,
    show_default=False,
    help="Run MetaMobilePicker with a small test set"
)
@click.option(
	"-S","--submit",
	is_flag=True,
	default=False,
	show_default=False,
	help="Submit the snakemake script to SLURM"
)
@click.option(
	"-C","--cores",
	default=8,
	show_default=True,
	help="Specify the number of available cores"
)
def run_pipeline(dryrun, sbatch, profile, config,snakefile, test,unlock,submit, cores=1):
    # TODO: Add checks for input files here
    # cores is set to 1 to please Snakemake (multithreading not yet supported)
    file_2 = "../data/raw/ERR2241639_2_test.fastq.gz"  
    file_1 = "../data/raw/ERR2241639_1_test.fastq.gz"
    if test and os.path.exists(file_2) and os.path.exists(file_1):
        config = "config/test_config.yaml"
    if test and (not os.path.exists(file_2) or not os.path.exists(file_1)):
        print("You haven't downloaded any test data. Please run 'metamobilepicker.py download' first")
        exit(1)

    # Base command
    cmd = (
        "snakemake --use-conda --snakefile {snakefile} --cores {cores} -j 10\
        --configfile {config} --rerun-incomplete  --latency-wait 60 --use-singularity\
        --singularity-args '--home $PWD/.. --bind $TMPDIR,$PWD/..'"
        ).format(snakefile=snakefile, cores=cores, config=config)
    if dryrun:
        cmd += " --dryrun"
    if unlock:
        cmd += " --unlock"
    # Profile not yet implemented
    if profile != None:
        cmd += " {} ".format(profile)
    if sbatch:
        cmd += ' --profile ../slurm/'
        #cmd += ' --cluster "sbatch -t 18:00:00 -N 1 -c 20 --mem-per-cpu 12G --verbose"'
        runname = getRunName("../docs/log", callname="MetaMobilePicker_slurms")
        #subprocess.call(["python","./scripts/demessifier.py","arm","-d","../docs/log", "-n","MetaMobilePicker_slurms"])
    if sbatch and submit:
        cmd = "sbatch -J MetaMobilePicker -t 48:00:00 -c 1 --mem-per-cpu 16G --gres=tmpspace:64G  {}".format(cmd)
    elif submit and not sbatch:
        logging.critical("--submit can only be used in combination with --sbatch")
        exit(1)
    
    # Run the pipeline
    try:
        subprocess.check_call(cmd,shell=True)
        #if sbatch:
        #    subprocess.call(["python","./scripts/demessifier.py","clean","-n",runname,"--cleaning","slurm"])
        goodbye()
        exit(0)
    except subprocess.CalledProcessError as error:
        if sbatch and not submit:
            subprocess.call(["python","./scripts/demessifier.py","clean","-n",runname,"--cleaning","slurm"])
        logging.critical(error)
        if sbatch and submit:
            print("Please run the demessifier script with {} as the runname, as this can't be done automatically with the -S flag".format(runname))
        exit(1)

if __name__ == "__main__":
    cligroup()
