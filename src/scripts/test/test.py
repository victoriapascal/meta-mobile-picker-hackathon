#!/bin/bash python

import shutil
import os
import sys
import subprocess

root_wd = os.getcwd()
os.chdir(root_wd+"/src/")
src_wd = os.getcwd()
shutil.copy (root_wd+"/data/test/test_reads_10k_R1.fastq", src_wd)
shutil.copy (root_wd+"/data/test/test_reads_10k_R2.fastq", src_wd)
commandstring = ['python','metamobilepicker.py','run','--config',"{}/src/config/CI_config.yaml".format(root_wd)]
print(' '.join(commandstring))
process = subprocess.Popen(commandstring, stdout=subprocess.PIPE)
#shutil.copy (cwd+"/src/config/test_config.yaml",cwd+"/src/config/config.yaml")
#process = subprocess.Popen(['python','metamobilepicker.py',
#'run',"--config",f"{root_wd}/src/config/CI_config.yaml"], 
#stdout=subprocess.PIPE)

stdout = process.communicate()[0]
print('STDOUT:{}'.format(stdout))
rc = process.returncode
os.remove ("./test_reads_10k_R1.fastq")
os.remove ("./test_reads_10k_R2.fastq")
sys.exit(rc)

