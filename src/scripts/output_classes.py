#!/usr/bin/python
import textwrap
from random import randint

class Contig:
    """
    Contig class describes contains information about each contig
    in the input fasta.
    """
    def __init__(self,name,length):
        """
        Method initiates object
        """
        self.has_mges = False
        self.length = length
        self.MGEs = []
        self.annotations = []
        self.name = name
    def getName(self):
        """ Returns name of contig (fasta header)"""
        return(self.name)

    def getLength(self):
        """Returns length of contig """
        return(self.length)

    def addMGE(self, mge):
        """ 
        Adds MGE object to list of MGEs
        If no other MGEs exist, it changes the condition of the contig
        to 'containing at least one MGE'
        """
        if not self.has_mges:
            self.has_mges = True
        self.MGEs.append(mge)
   
    def getMGE(self,type="names"):
        """
        Returns the name of all MGEs by default. 
        Returns the list of MGE objects if type parameter is given
        """
        if type == "names":
            return([x.getName() for x in self.MGEs])
        else:
            return(self.MGEs)
    
    def addAnnotation(self, annotation):
        """
        Adds annotation object to list of annotation objects
        """
        self.annotations.append(annotation)

    def printHierarchy(self):
        """
        Constructs the hierarchical output for the contig
        """
        mge_strings = ""
        outstring = textwrap.dedent("""\
        contig: 
            contig_id: {}
            length: {}
            """)
       
        if not self.has_mges:
            return None # Only contigs with MGEs are needed
        
        # Sorting the MGEs and annotations
        elements = self.MGEs + self.annotations # Annotations and MGEs are combined
        # Elements are given three numerical values: start, stop and (arbitrary) MGE priority
        # To avoid errors when two elements start and stop at the same place, a priority is given to the types of elements
        elements = [[x.getStart(), 1-x.getStop(), x.getPriority(), x] for x in elements] 
        elements.sort(key=lambda x: x[0])

        # An output string is generated for each element
        for i in elements:
            element = i[3]
            annstring = ""
            start = element.getStart()
            stop = element.getStop()
            newstring = element.getHierarchy()
            mge_strings += newstring
        fullstring = outstring.format(self.name,self.length)+textwrap.indent(mge_strings,prefix="    ")
        return(fullstring)


class MGE:
    """
    MGE class containing general MGE information 
    """
    def __init__(self,start,stop,type,name=None,annotation=None, resistance=None, taxonomy = None):
        """ 
        Method initiates the MGE objects
        """
        self.start = start
        self.name = name
        self.stop = stop
        self.type = type
        self.annotation = annotation
        self.resistance = resistance
        self.prior = {"plasmid":1,"phage":2,"Tn":3,"IS":4}
        self.fields = {"plasmid":["ID","score"],"phage":["ID","q_value"],"IS":["ID","class","start","stop"],"resistance":["ID","name","start","stop","source","gene","accession"]}
 
    def getAnnotation(self):
        """ Returns annotation of MGE """
        return(self.annotation)

    def setAnnotation(self, annotation):
        """ Sets annotation after MGE has been initiated """
        self.annotation = annotation

    def getResistance(self):
        """ Returns resistance gene of MGE """
        return(resistance)

    def setResistance(self,resistance):
        """ Sets MGE's resistance gene after initiation """
        self.resistance = resistance

    def getType(self):
        """ Returns type of MGE """
        return(self.type)

    def getStart(self):
        """ Returns start position as int """
        return(int(self.start))

    def getStop(self):
        """ Returns stop position as int """
        return(int(self.stop))

    def getLength(self):
        """ Returns the length of the MGE """
        return(self.stop-self.start)

    def getHierarchy(self):
        """ Returns hierarchical string for outputing """
        # TODO: make it possible for different MGEs to add different fields
        type = self.type
        fields = self.fields[type]
        #print(fields)
        outstring = f"{type}:\n    "
        
        #for field in fields:
        #    outstring.append("    {}: 
        outstring = textwrap.dedent("""\
                         {}:
                             name: {}
                             start: {}
                             stop: {}
                     """)
        return(outstring.format(self.type,self.name,self.start,self.stop))

    def getPriority(self):
        """ Returns priority level (order of printing) based on type """
        return(self.prior[self.type])

class Plasmid(MGE):
    """ Class extends the MGE class with plasmid specific attributes"""
    def __init__(self,name,length, taxonomy=None,score="NA"):
        """ Initiates the plasmid object """
        type = "plasmid"
        MGE.__init__(self,1,length,type,name, taxonomy=taxonomy)
        self.name = name
        self.taxonomy = taxonomy
        self.score = score

    def getName(self):
        """ Returns name """
        return(self.name)

    def getHierarchy(self):
        """ returns plasmid-specific hierarchy """
        outstring = textwrap.dedent(f"""\
                         plasmid:
                             ID:{self.name}
                             score:{self.score}
                    """)
        return(outstring)
        
class Phage(MGE):
    """ Class extends MGE with (the possibility of) Phage specific attributes"""

    def __init__(self, length,name, score="NA"):
        """ initiates phage object and corresponding MGE object """
        type = "phage"
        self.name = name
        self.score = score
        MGE.__init__(self, 1, length, type, name)

    def getHierarchy(self):

        outstring = textwrap.dedent(f"""\
                         phage: 
                             ID:{self.name}
                             score:{self.score}
                    """)

        return(outstring)

class IS(MGE):
    """ Class extends MGE with (the possibility of) IS specific attributes"""
    def __init__(self, start, stop, name):
        """ initiates phage object and corresponding MGE object """
        self.start = start
        self.stop = stop
        self.name= name
        type= "IS"
        MGE.__init__(self, self.start, self.stop, type, self.name)

    def getHierarchy(self):
        #["ID","class","start","stop"]
        outstring = textwrap.dedent(f"""\
                         IS:
                             ID: {self.name}
                             class: {self.name}
                             start: {self.start}
                             stop: {self.stop}
                    """)
        return(outstring)
                         
        
class Annotation():
    ## TODO: change name of MGE class because annotations should inherit that class
    # #FILE   SEQUENCE        START   END     STRAND  GENE    COVERAGE        COVERAGE_MAP    GAPS    %COVERAGE %IDENTITY	DATABASE        ACCESSION	PRODUCT RESISTANCE


    def __init__(self, start,stop, name, source, gene, accession):
        self.start = start
        self.stop = stop
        self.name = name
        self.source = source
        self.gene = gene
        self.accession = accession
        self.added = False

    def getHierarchy(self):
        outstring = textwrap.dedent("""\
                         Annotation:
                             name: {}
                             start: {}
                             stop: {}
                             source: {}
                             gene: {}
                             accession: {}
                     """)

        return(outstring.format(self.name,self.start,self.stop, self.source, self.gene, self.accession))

    def getPos(self):
        return([self.start,self.stop])
    def setAdded(self):
        self.added = not self.added
    def getAdded(self):
        return(self.added)
    def getStart(self):
        return(int(self.start))
    def getStop(self):
        return(int(self.stop))
    def getPriority(self):
        return(9)
if __name__ == "__main__":
    # make a contig
    contigje = Contig("Contig_1",100000)
    
    # make a plasmid to go onto the contig
    plasmid_length = contigje.getLength()
    plasmidje = Plasmid("testplasmid",plasmid_length, "Firmicutes")

    # make a transposon to go onto the contig
    #transposon = MGE(123,45678,"Tn","Tn4")
    contigje.addMGE(plasmidje)
    #contigje.addMGE(transposon)
    print(contigje.getMGE("all"))
    
    # printing out the hierarchy (e.g.)
    print(contigje.printHierarchy())
    
    # adding a phage
    phageje = Phage(12345, "phage_1",score=0.99)
    contigje.addAnnotation(phageje)

    # Adding an annotation
    annotationtje = Annotation(123,3245,name= "Abricate", source="ResFinder", gene= "Resistance Gene A", accession = "RGA1234")
    contigje.addAnnotation(annotationtje)

    # Adding an IS
    isje = IS(1234,4567, name= "IS3")
    contigje.addMGE(isje) 
    print(contigje.printHierarchy())
    print("-----------------------------------------------")
    #print(contigje.printHierarchyTwo())

   
    
   
